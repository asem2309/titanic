import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    result = []

    prefixes = ["Mr.", "Mrs.", "Miss."]

    for p in prefixes:
        df_p = df[df['Name'].str.contains(p)]
        median = round(df_p['Age'].median())
        n_missing = df_p['Age'].isnull().sum()

        result.append((p, n_missing, median))

    return result
